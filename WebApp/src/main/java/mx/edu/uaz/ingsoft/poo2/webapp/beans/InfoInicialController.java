package mx.edu.uaz.ingsoft.poo2.webapp.beans;

import java.io.Serializable;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;

@Named(value = "infoInicialController")
@SessionScoped
public class InfoInicialController implements Serializable {
    
    private static final long serialVersionUID = 1L;
    
    @Inject
    UsuarioController usuarioController;
    
    public InfoInicialController(){       
    }
    
    public Integer totalUsuario(){
        return usuarioController.getTotalRegistros();
    }
    
}