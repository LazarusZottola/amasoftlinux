package mx.edu.uaz.ingsoft.poo2.webapp.beans.interfaces;

/**
 *
 * @author eibarra
 */
public interface IEntidadBaseController<T> {
    
    String create();
    String delete();
    String update();
    void findAll();
    int getTotalRegistros();
    String findByID();
    
}
