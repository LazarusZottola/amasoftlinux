package mx.edu.uaz.ingsoft.poo2.webapp.beans;

import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import mx.edu.uaz.ingsoft.poo2.persistencia.dao.UsuarioDAO;
import mx.edu.uaz.ingsoft.poo2.persistencia.entidades.Usuario;
import mx.edu.uaz.ingsoft.poo2.webapp.beans.utils.MensajesPrograma;

/**
 *
 * @author leo
 */
@Named("UsuarioController")
@RequestScoped
public class UsuarioController extends EntidadBaseController<Usuario> implements Serializable{

    private static final long serialVersionUID = 1L;
    
    private  UsuarioDAO dao;
    private  String mensajeError;
    private int idEntidadBuscada;
    
    @PostConstruct
    public void initUsuarioController(){
        dao = new UsuarioDAO(em);
        setEntidadLocal(new Usuario());
        setListaEntidades(dao.findAll());
        mensajeError=MensajesPrograma.MSG_VACIO;
        
    }
    public List<Usuario> getListadoUsuario(){
        return getListaEntidades();
    }
 
    public int getIdEntidadBuscada() {
        return idEntidadBuscada;
    }

 
    public void setIdEntidadBuscada(int idEntidadBuscada) {
        this.idEntidadBuscada = idEntidadBuscada;
    }
    
    @Override
    public int getTotalRegistros(){
        return  dao.findAll().size();
    }

    @Override
    public String create() {
        if(getEntidadLocal().getNombre().trim().isEmpty() && getEntidadLocal().getCorreo().trim().isEmpty() && getEntidadLocal().getContrasenia().trim().isEmpty()){
            mensajeError=MensajesPrograma.MSG_DATOS_INCOMPLETOS;
            return "";
        }
        
        dao.add(getEntidadLocal());
        
        Usuario unUsuario = dao.findByID(getEntidadLocal().getId());
        if(unUsuario == null){
            mensajeError=MensajesPrograma.MSG_VACIO;
            return "";

        }else{
            setEntidadLocal(new Usuario());
            setListaEntidades(dao.findAll());
            return "listado";
        }
    }
        
    @Override
    public void findAll() {
    
        setListaEntidades(dao.findAll());
    }
    @Override
    public String findByID() {
        setEntidadLocal(dao.findByID(idEntidadBuscada));
        if(getEntidadLocal() != null){
            idEntidadBuscada =0;
            return "mostrarId";            
        }else{
          mensajeError=MensajesPrograma.MSG_DATOS_INCOMPLETOS;
          return "";
        }
    }    

    @Override
    public String update() {
        setEntidadLocal(dao.findByID(idEntidadBuscada));
        if(getEntidadLocal() != null){
            idEntidadBuscada =0;

            return "busActualizar";
        }else{
            mensajeError=MensajesPrograma.MSG_DATOS_INCOMPLETOS;
            return "";
        }
    }
    
    public String actualizar(){    
        dao.update(getEntidadLocal());
        setListaEntidades(dao.findAll());
        return "listado";

    }
    
    @Override
    public String delete() {
        Usuario unUsuario = dao.findByID(getEntidadLocal().getId());
        if(unUsuario != null){
            dao.delete(unUsuario);
            setListaEntidades(dao.findAll());
            return "listado";
        }else{
            mensajeError=MensajesPrograma.MSG_DATOS_INCOMPLETOS;
            return "";
        }
    }
    public String getMensajeError() {
        return mensajeError;
    }
    public String regresarListado(){
        setListaEntidades(dao.findAll());
        return "listado";
    }
    public String listaU(){
        setListaEntidades(dao.findAll());
        return "listado";
    }

}

