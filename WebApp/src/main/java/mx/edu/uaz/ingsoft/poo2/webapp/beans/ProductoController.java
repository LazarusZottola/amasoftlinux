/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.edu.uaz.ingsoft.poo2.webapp.beans;

import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import mx.edu.uaz.ingsoft.poo2.persistencia.dao.ProductoDAO;
import mx.edu.uaz.ingsoft.poo2.persistencia.entidades.Producto;
import mx.edu.uaz.ingsoft.poo2.webapp.beans.utils.MensajesPrograma;

/**
 *
 * @author juan
 */
@Named("ProductoController")
@SessionScoped

public class ProductoController extends EntidadBaseController<Producto> implements Serializable{
    private static final long serialVersionUID = 1L;
    
    private  ProductoDAO dao;
    private  String mensajeError;
    private int idEntidadBuscada;
    
    @PostConstruct
    public void initProductoController(){
        dao = new ProductoDAO(em);
        setEntidadLocal(new Producto());
        setListaEntidades(dao.findAll());
        mensajeError=MensajesPrograma.MSG_VACIO;    
    }
    public List<Producto> getListadoUsuario(){
        return getListaEntidades();
    }
    public int getIdEntidadBuscada() {
        return idEntidadBuscada;
    }
    public void setIdEntidadBuscada(int idEntidadBuscada) {
        this.idEntidadBuscada = idEntidadBuscada;
    }
    
    @Override
    public int getTotalRegistros(){
        return  dao.findAll().size();
    }

    @Override
    public String create() {
        if(getEntidadLocal().getNombre().trim().isEmpty() && 
                getEntidadLocal().getDescripcion().trim().isEmpty() && 
                Float.toString(getEntidadLocal().getPrecioCompra()).trim().isEmpty() &&
                Float.toString(getEntidadLocal().getPrecioVenta()).trim().isEmpty()){
            mensajeError=MensajesPrograma.MSG_DATOS_INCOMPLETOS;
            return "";
        }
        
        dao.add(getEntidadLocal());
        
        Producto producto = dao.findByID(getEntidadLocal().getId());
        if(producto == null){
            mensajeError=MensajesPrograma.MSG_VACIO;
            return "";

        }else{
            setEntidadLocal(new Producto());
            setListaEntidades(dao.findAll());
            return "list";
        }
    }
        
    @Override
    public void findAll() {
        setListaEntidades(dao.findAll());
    }
    
    @Override
    public String findByID() {
        setEntidadLocal(dao.findByID(idEntidadBuscada));
        if(getEntidadLocal() != null){
            idEntidadBuscada = 0;
            return "findId";            
        }else{
          mensajeError=MensajesPrograma.MSG_VACIO;
          return "";
        }
    }    

    @Override
    public String update() {
        setEntidadLocal(dao.findByID(idEntidadBuscada));
        if(getEntidadLocal() != null){
            idEntidadBuscada = 0;
            return "findUp";
        }else{
            mensajeError=MensajesPrograma.MSG_VACIO;
            return "";
        }
    }

    public String up(){    
        dao.update(getEntidadLocal());
        setListaEntidades(dao.findAll());
        return "list";
    }

    @Override
    public String delete() {
        Producto producto = dao.findByID(getEntidadLocal().getId());
        if(producto != null){
            dao.delete(producto);
            setListaEntidades(dao.findAll());
            return "list";
        }else{
            mensajeError=MensajesPrograma.MSG_VACIO;
            return "";
        }
    }
    
     /**
     * @return the mensajeError
     */
    public String getMensajeError() {
        return mensajeError;
    }
    public String regresarListado(){
        setListaEntidades(dao.findAll());
        return "list";
    }
}
