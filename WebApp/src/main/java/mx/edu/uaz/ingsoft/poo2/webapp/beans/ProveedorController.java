package mx.edu.uaz.ingsoft.poo2.webapp.beans;

import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import mx.edu.uaz.ingsoft.poo2.persistencia.dao.ProveedorDAO;
import mx.edu.uaz.ingsoft.poo2.persistencia.entidades.Proveedor;
import mx.edu.uaz.ingsoft.poo2.webapp.beans.utils.MensajesPrograma;

/**
 *
 * @author eibarra
 */
@Named("ProveedorController")
@SessionScoped
public class ProveedorController extends EntidadBaseController<Proveedor> implements Serializable{
    private static final long serialVersionUID = 1L;
    
    private  ProveedorDAO dao;
    private  String mensajeError;
    private int idEntidadBuscada;
    
    @PostConstruct
    public void initProveedorController(){
        dao = new ProveedorDAO(em);
        setEntidadLocal(new Proveedor());
        setListaEntidades(dao.findAll());
        mensajeError=MensajesPrograma.MSG_VACIO;    
    }
    public List<Proveedor> getListadoUsuario(){
        return getListaEntidades();
    }
    public int getIdEntidadBuscada() {
        return idEntidadBuscada;
    }
    public void setIdEntidadBuscada(int idEntidadBuscada) {
        this.idEntidadBuscada = idEntidadBuscada;
    }
    
    @Override
    public int getTotalRegistros(){
        return  dao.findAll().size();
    }

    @Override
    public String create() {
        if(getEntidadLocal().getNombre().trim().isEmpty() && 
                getEntidadLocal().getTelefono().trim().isEmpty() && 
                getEntidadLocal().getDireccion().trim().isEmpty() &&
                getEntidadLocal().getCiudad().trim().isEmpty() &&
                getEntidadLocal().getEmail().trim().isEmpty() &&
                getEntidadLocal().getProductos().trim().isEmpty()){
            mensajeError=MensajesPrograma.MSG_DATOS_INCOMPLETOS;
            return "";
        }
        
        dao.add(getEntidadLocal());
        
        Proveedor proveedor = dao.findByID(getEntidadLocal().getId());
        if(proveedor == null){
            mensajeError=MensajesPrograma.MSG_VACIO;
            return "";

        }else{
            setEntidadLocal(new Proveedor());
            setListaEntidades(dao.findAll());
            return "list";
        }
    }
        
    @Override
    public void findAll() {
        setListaEntidades(dao.findAll());
    }
    
    @Override
    public String findByID() {
        setEntidadLocal(dao.findByID(idEntidadBuscada));
        if(getEntidadLocal() != null){
            idEntidadBuscada =0;
            // solucionar el mostrar en la misma pantalla
            return "list";            
        }else{
          mensajeError=MensajesPrograma.MSG_VACIO;
          return "";
        }
    }    

    @Override
    public String update() {
        Proveedor proveedor = dao.findByID(getEntidadLocal().getId());
        if(proveedor != null){
            dao.update(proveedor);
            return "list";
        }else{
            mensajeError=MensajesPrograma.MSG_DATOS_INCOMPLETOS;
            return "";
        }
    }

    @Override
    public String delete() {
        Proveedor proveedor = dao.findByID(getEntidadLocal().getId());
        if(proveedor != null){
            dao.delete(proveedor);
            setListaEntidades(dao.findAll());
            return "list";
        }else{
            mensajeError=MensajesPrograma.MSG_VACIO;
            return "";
        }
    }
    
     /**
     * @return the mensajeError
     */
    public String getMensajeError() {
        return mensajeError;
    }
    public String regresarListado(){
        setListaEntidades(dao.findAll());
        return "list";
    }
}
