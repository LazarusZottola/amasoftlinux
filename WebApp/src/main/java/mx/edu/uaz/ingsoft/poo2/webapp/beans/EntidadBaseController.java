package mx.edu.uaz.ingsoft.poo2.webapp.beans;

import mx.edu.uaz.ingsoft.poo2.webapp.beans.interfaces.IEntidadBaseController;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author eibarra
 * @param <T>
 */
public abstract class EntidadBaseController<T> implements IEntidadBaseController<T> {
    
    private final EntityManagerFactory emf;
    protected final EntityManager em; 
    
    private T entidadLocal;
    private List<T> listaEntidades;
    
    public EntidadBaseController(){
        emf = Persistence.createEntityManagerFactory("UnidadPersistenciaAmasoft");
        em=emf.createEntityManager();
    }

    public T getEntidadLocal() {
        return entidadLocal;
    }

    public void setEntidadLocal(T entidadLocal) {
        this.entidadLocal = entidadLocal;
    }

    public List<T> getListaEntidades() {
        return listaEntidades;
    }

    public void setListaEntidades(List<T> listaEntidades) {
        this.listaEntidades = listaEntidades;
    }
    
}
