package mx.edu.uaz.ingsoft.poo2.webapp.beans;

import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import mx.edu.uaz.ingsoft.poo2.persistencia.dao.CompraDAO;
import mx.edu.uaz.ingsoft.poo2.persistencia.entidades.Compra;
import mx.edu.uaz.ingsoft.poo2.webapp.beans.utils.MensajesPrograma;

/**
*
* @author aldo & Jaqueline
*/

@Named("CompraController")
@SessionScoped
public class CompraController extends EntidadBaseController<Compra> implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private CompraDAO dao;
	private String mensajeError;
    private int idEntidadBuscada;

	@PostConstruct
	public void initCompraController() {
		dao = new CompraDAO(em);
        setEntidadLocal(new Compra());
        setListaEntidades(dao.findAll());
        mensajeError=MensajesPrograma.MSG_VACIO;
	}

	public List<Compra> getListadoCompras() {
		return getListaEntidades();
	}
        
    public int getIdEntidadBuscada() {
        return idEntidadBuscada;
    }
        
    public void setIdEntidadBuscada(int idEntidadBuscada) {
        this.idEntidadBuscada = idEntidadBuscada;
    }

	@Override
	public int getTotalRegistros() {
		return dao.findAll().size();
	}

	@Override
	public String create() {
        if(getEntidadLocal().getPrecioFinal() == 0.00 && 
                getEntidadLocal().getProductos() == null){

            mensajeError=MensajesPrograma.MSG_DATOS_INCOMPLETOS;
            return "";
        }
        
        dao.add(getEntidadLocal());
        
        Compra compra = dao.findByID(getEntidadLocal().getId());
        if(compra == null){
            mensajeError=MensajesPrograma.MSG_VACIO;
            return "";

        }else{
            setEntidadLocal(new Compra());
            setListaEntidades(dao.findAll());
            return "list";
        }
	}

	@Override
	public void findAll() {
		setListaEntidades(dao.findAll());
	}

	@Override
	public String update() {
		setEntidadLocal(dao.findByID(idEntidadBuscada));
        if(getEntidadLocal() != null){
            idEntidadBuscada = 0;
            return "findUp";
        }else{
            mensajeError=MensajesPrograma.MSG_VACIO;
            return "";
        }
	}

	public String up(){    
        dao.update(getEntidadLocal());
        setListaEntidades(dao.findAll());
        return "list";
        }

	@Override
	public String delete() {
		Compra compra = dao.findByID(getEntidadLocal().getId());

		if (compra != null) {
			dao.delete(compra);
			setListaEntidades(dao.findAll());
			return "list";
		} else {
			mensajeError = MensajesPrograma.MSG_VACIO;
			return "";
		}
	}
	/**
	*@return the mensajeError
	*/
	public String getMensajeError() {
		return mensajeError;
	}

	public String regresarListado() {
		setListaEntidades(dao.findAll());
		return "list";
	}

        @Override
        public String findByID() {
            setEntidadLocal(dao.findByID(idEntidadBuscada));
            if(getEntidadLocal() != null){
                idEntidadBuscada = 0;
                return "findId";            
            }else{
                mensajeError=MensajesPrograma.MSG_VACIO;
                return "";
        }
    } 
}