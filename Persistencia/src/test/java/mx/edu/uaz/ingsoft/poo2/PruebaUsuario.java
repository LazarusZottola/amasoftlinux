package mx.edu.uaz.ingsoft.poo2;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import mx.edu.uaz.ingsoft.poo2.persistencia.dao.UsuarioDAO;
import mx.edu.uaz.ingsoft.poo2.persistencia.entidades.Usuario;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author leo_c
 */
public class PruebaUsuario {
    static Logger logger = Logger.getLogger(UsuarioDAO.class.getName());
    
    private static EntityManagerFactory emf;
    private static EntityManager em;    
    private static UsuarioDAO dao;
    private static int numUsuario;

    
    public PruebaUsuario() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        emf = Persistence.createEntityManagerFactory("UnidadPersistenciaAmasoft");
        em=emf.createEntityManager();
        dao = new UsuarioDAO(em);

    }
    
    @AfterClass
    public static void tearDownClass() {
        em.close();
        emf.close();
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testAdd() {
        Usuario entidad = crearNuevoUsuario();
        entidad = dao.add(entidad);
        assertNotNull(entidad);
        assertNotNull(entidad.getId());
        logger.info(entidad.toString());        
    }
    @Test
    public void testFindByID(){
        Usuario entidad = crearNuevoUsuario();
        assertNull(entidad.getId());
        dao.add(entidad);
        assertNotNull(entidad.getId());
        logger.info("Recuperando=>"+entidad.toString());
        Usuario recuperado = dao.findByID(entidad.getId());
        assertNotNull(recuperado);
        assertEquals(entidad.getNombre(),recuperado.getNombre());
        dao.delete(entidad);

    }
    @Test
    public void testFindAll(){
        Usuario entidad = crearNuevoUsuario();
        Usuario entidad2 = crearNuevoUsuario();
        dao.add(entidad);
        dao.add(entidad2);
        List<Usuario> listaRecuperada;
        listaRecuperada = dao.findAll();
        assertNotNull(listaRecuperada);
        assert(listaRecuperada.size() >= 2);
        dao.delete(entidad);
        dao.delete(entidad2);


    }
    @Test
    public void testDelete(){
        Usuario entidad = crearNuevoUsuario();
        assertNull(entidad.getId());
        entidad = dao.add(entidad);
        assertNotNull(entidad.getId());
        Integer idRegistro = entidad.getId();
        logger.log(Level.INFO, "Borrando=>{0}", entidad.toString());
        dao.delete(entidad);
        Usuario recuperado;
        recuperado = dao.findByID(idRegistro);
        assertNull(recuperado);
    }
    @Test
    public void testUpdate(){
        Usuario entidad = crearNuevoUsuario();
        entidad = dao.add(entidad);

        Usuario ucopy = new Usuario();
        ucopy.setId(entidad.getId());
        ucopy.setNombre(entidad.getNombre());
        ucopy.setCorreo(entidad.getCorreo());
        ucopy.setContrasenia(entidad.getContrasenia());

        Usuario entidad2 = crearNuevoUsuario();
        entidad2.setNombre("Francisco de la Merced");
        entidad2.setCorreo("franco@hotmail.com");
        entidad2.setContrasenia("estaesmicontraseña");

        assertEquals(entidad, ucopy );
        logger.log(Level.INFO, "Actualizando=>", entidad.toString());
        dao.update(entidad2);
        assertNotEquals(ucopy,entidad2);
        dao.delete(entidad);
        
    }
    

    private static Usuario crearNuevoUsuario(){

        Usuario nuevo = new Usuario();
        nuevo.setNombre("Juan Carlos Bodoque");
        nuevo.setCorreo("boque@hotmail.com");
        nuevo.setContrasenia("bodoque1234567");

        return nuevo;
    }
}