package mx.edu.uaz.ingsoft.poo2;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import mx.edu.uaz.ingsoft.poo2.persistencia.entidades.Proveedor;
import mx.edu.uaz.ingsoft.poo2.persistencia.dao.ProveedorDAO;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author eibarra
 */
public class PruebaProveedor {
    
    static final Logger logger = Logger.getLogger(ProveedorDAO.class.getName());
    
    private static EntityManagerFactory emf;
    private static EntityManager em;    
    private static ProveedorDAO dao;
    
    private static final String ACTUALIZAR_NOMBRE="Juan Baez";
    private static final String ACTUALIZAR_TELEFONO="+52 496 910 9873";
    private static final String ACTUALIZAR_EMAIL="juanbaeh@gmail.com";

    public PruebaProveedor() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        emf = Persistence.createEntityManagerFactory("UnidadPersistenciaAmasoft");
        em=emf.createEntityManager();
        dao = new ProveedorDAO(em);
    }
    
    @AfterClass
    public static void tearDownClass() {
        em.close();
        emf.close();
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }
    
    @Test
    public void testAdd() {
        Proveedor entidad = crearProveedor();
        entidad = dao.add(entidad);
        assertNotNull(entidad);
        assertNotNull(entidad.getId());
        logger.info(entidad.toString());
    }
    
    @Test
    public void testDelete(){
        Proveedor entidad = crearProveedor();
        assertNull(entidad.getId());
        entidad = dao.add(entidad);
        assertNotNull(entidad.getId());
        Integer idRegistro = entidad.getId();
        logger.log(Level.INFO, "Borrando=> "+entidad.toString());
        dao.delete(entidad);
        Proveedor recuperado;
        recuperado = dao.findByID(idRegistro);
        assertNull(recuperado);
    }
    
    @Test
    public void testUpdate(){
        Proveedor entidad = crearProveedor();
        entidad = dao.add(entidad);
        Integer idOriginal = entidad.getId();
        entidad.setNombre(ACTUALIZAR_NOMBRE);
        entidad.setTelefono(ACTUALIZAR_TELEFONO);
        entidad.setEmail(ACTUALIZAR_EMAIL);
        logger.log(Level.INFO, "Actualizando=> "+entidad.toString());
        dao.update(entidad);
        Proveedor recuperado = dao.findByID(idOriginal);
        assertNotNull(recuperado);
        assert(entidad.getEmail().equals(recuperado.getEmail()));
    }
    
    @Test
    public void testFindByID(){
        Proveedor entidad = crearProveedor();
        assertNull(entidad.getId());
        dao.add(entidad);
        assertNotNull(entidad.getId());
        logger.info("Recuperando=> "+entidad.toString());
        Proveedor recuperado = dao.findByID(entidad.getId());
        assertNotNull(recuperado);
        assertEquals(entidad.getEmail(),recuperado.getEmail());
    }
    
    @Test
    public void testFindAll(){
        dao.add(crearProveedor());
        dao.add(crearProveedor());
        List<Proveedor> listaRecuperada;
        listaRecuperada = dao.findAll();
        assertNotNull(listaRecuperada);
        assert(listaRecuperada.size() >= 2);
        System.out.println(listaRecuperada);
    }
    
    public Proveedor crearProveedor() {
        Proveedor proveedor = new Proveedor();
        proveedor.setNombre("Emmanuel Ibarra");
        proveedor.setTelefono("+52 492 102 9683");
        proveedor.setDireccion("Pancho Villa #509");
        proveedor.setCiudad("Aguascalientes");
        proveedor.setEmail("emmanuel@gmail.com");
        proveedor.setNombreEmpresa("Software Guru");
        proveedor.setProductos("Laptop, MousePad, Audifono, Disco duro");
        return proveedor;
    }
    
}