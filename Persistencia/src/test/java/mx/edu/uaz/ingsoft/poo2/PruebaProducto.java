/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.edu.uaz.ingsoft.poo2;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import mx.edu.uaz.ingsoft.poo2.persistencia.dao.ProductoDAO;
import mx.edu.uaz.ingsoft.poo2.persistencia.entidades.Producto;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import java.util.List;

/**
 *
 * @author JuanPablo
 */
public class PruebaProducto {
    static Logger logger = Logger.getLogger(ProductoDAO.class.getName());
    
    private static EntityManagerFactory emf;
    private static EntityManager em;
    private static ProductoDAO dao;
    private int numProducto;
    
    public PruebaProducto(){
    }
        
    @BeforeClass
    public static void setUpClass() {
        emf = Persistence.createEntityManagerFactory("UnidadPersistenciaAmasoft");
        em=emf.createEntityManager();
        dao = new ProductoDAO(em);
    }
    @AfterClass
    public static void tearDownClass() {
        em.close();
        emf.close();
    }
        
    @Before
        public void setUp() {
    }
        
    @After
    public void tearDown() {
    }
    @Test
    public void testAdd() {
        Producto entidad = crearNuevoProducto();
        entidad = dao.add(entidad);
        assertNotNull(entidad);
        assertNotNull(entidad.getId());
        logger.info(entidad.toString());
        dao.delete(entidad);
    }

    @Test
    public void testDelete() throws Exception{
        Producto entidad = crearNuevoProducto();
        assertNull(entidad.getId());
        entidad = dao.add(entidad);
        assertNotNull(entidad.getId());
        Integer idRegistro = entidad.getId();
        logger.log(Level.INFO, "Borrando=>{0}", entidad.toString());
        dao.delete(entidad);
        Producto recuperado;
        recuperado = dao.findByID(idRegistro);
        assertNull(recuperado);
    }
    @Test
    public void testFindByID(){
        Producto entidad = crearNuevoProducto();
        assertNull(entidad.getId());
        dao.add(entidad);
        assertNotNull(entidad.getId());
        logger.info("Recuperando =>"+entidad.toString());
        Producto recuperado = dao.findByID(entidad.getId());
        assertNotNull(recuperado);
        assertEquals(entidad.getNombre(),recuperado.getNombre());
        dao.delete(entidad);
    }
    
    @Test
    public void testFindAll(){
        Producto entidad = crearNuevoProducto();
        Producto entidad2 = crearNuevoProducto();
        dao.add(entidad);
        dao.add(entidad2);
        List<Producto> listaRecuperada;
        listaRecuperada = dao.findAll();
        assertNotNull(listaRecuperada);
        assert(listaRecuperada.size() >= 2);
        dao.delete(entidad);
        dao.delete(entidad2);
    }
    @Test
    public void testUpdate(){
        Producto entidad = crearNuevoProducto();
        entidad = dao.add(entidad);
        
        Producto copiaProducto = new Producto();
        copiaProducto.setId(entidad.getId());
        copiaProducto.setNombre(entidad.getNombre());
        copiaProducto.setDescripcion(entidad.getDescripcion());
        copiaProducto.setPrecioCompra(entidad.getPrecioCompra());
        copiaProducto.setPrecioVenta(entidad.getPrecioVenta());
        
        Producto entidad2 = crearNuevoProducto();
        entidad2.setNombre("Memoria USB");
        entidad2.setDescripcion("Capacidad 32GB, Marca Kingston");
        entidad2.setPrecioCompra(146.5f);
        entidad2.setPrecioVenta(215f);
        
        assertEquals(entidad, copiaProducto);
        logger.log(Level.INFO,"aCTUALIZANDO=>",entidad.toString());
        dao.update(entidad2);
        assertNotEquals(copiaProducto, entidad2);
        dao.delete(entidad);
    }

    private static Producto crearNuevoProducto(){
           Producto nuevo = new Producto();
            nuevo.setNombre("Audifonos");
            nuevo.setDescripcion("Marca Panasonic");
            nuevo.setPrecioCompra(34.05f);
            nuevo.setPrecioVenta(50.99f);
            return nuevo;
      } 
 }
 
