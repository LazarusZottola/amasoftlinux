package mx.edu.uaz.ingsoft.poo2;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import mx.edu.uaz.ingsoft.poo2.persistencia.dao.CompraDAO;
import mx.edu.uaz.ingsoft.poo2.persistencia.entidades.Compra;

import mx.edu.uaz.ingsoft.poo2.persistencia.entidades.Producto;
import mx.edu.uaz.ingsoft.poo2.persistencia.entidades.Usuario;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
*
*@author aldo
*/
public class PruebaCompra {
	static Logger logger = Logger.getLogger(CompraDAO.class.getName());

	private static EntityManagerFactory emf;
    private static EntityManager em;    
    private static CompraDAO dao;
    private static int numCompra;

    public PruebaCompra() {

    }

    @BeforeClass
    public static void setUpClass() {
		emf = Persistence.createEntityManagerFactory("UnidadPersistenciaAmasoft");
		em=emf.createEntityManager();
		dao = new CompraDAO(em);
    }

    @AfterClass
    public static void tearDownClass() {
    	em.close();
    	emf.close();
    }

	@Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testAdd(){
		Compra entidad = crearNuevaCompra();
		entidad = dao.add(entidad);
		assertNotNull(entidad);
		assertNotNull(entidad.getId());
		logger.info(entidad.toString());
		dao.delete(entidad);
    }   

    @Test
    public void pruebaBorrar() throws Exception {
		Compra entidad = crearNuevaCompra();
		assertNotNull(entidad.getId());
		entidad = dao.add(entidad);
		assertNotNull(entidad.getId());
		Integer idRegistro = entidad.getId();
		logger.log(Level.INFO, "Borrando=>{0}", entidad.toString());
		dao.delete(entidad);
		Compra recuperado;
		recuperado = dao.findByID(idRegistro);
		assertNull(recuperado);
    }

    @Test
    public void pruebaFindById() throws Exception {
		Compra entidad = crearNuevaCompra();
		assertNotNull(entidad.getId());
		dao.add(entidad);
		assertNotNull(entidad.getId());
		logger.info("Recuperando=>"+entidad.toString());
		Compra recuperado = dao.findByID(entidad.getId());
		assertNotNull(recuperado);
		assertEquals(entidad.getProductos(),recuperado.getProductos());
		dao.delete(entidad);
    }

    @Test
    public void pruebaFindAll() throws Exception {
		Compra entidad = crearNuevaCompra();
		Compra entidad2 = crearNuevaCompra();
		dao.add(entidad);
		dao.add(entidad2);
		List<Compra> listaRecuperada;
		listaRecuperada = dao.findAll();
		assertNotNull(listaRecuperada);
		assert(listaRecuperada.size() >= 2);
		dao.delete(entidad);
		dao.delete(entidad2);
    }

	@Test
	public void testUpdate(){
		Compra entidad = crearNuevaCompra();
		entidad = dao.add(entidad);

		Compra copiaCompra = new Compra();
		copiaCompra.setId(entidad.getId());
		copiaCompra.setProductos(entidad.getProductos());
		copiaCompra.setPrecioFinal(entidad.getPrecioFinal());

		Compra  entidad2 = crearNuevaCompra();
		entidad2.setProductos("Audifonos");
		entidad2.setPrecioFinal((float) 564.10);

		assertEquals(entidad, copiaCompra);
		logger.log(Level.INFO,"ACTUALIZANDO=>",entidad.toString());
		dao.update(entidad2);
		assertNotEquals(copiaCompra, entidad2);
		dao.delete(entidad);
	}

	private static Compra crearNuevaCompra(){
		Compra nuevo = new Compra();
		nuevo.setPrecioFinal((float) 284973.90);
		nuevo.setProductos("Audifonos");
		return nuevo;
	}

}

