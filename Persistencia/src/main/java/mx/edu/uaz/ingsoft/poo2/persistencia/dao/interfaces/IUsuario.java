package mx.edu.uaz.ingsoft.poo2.persistencia.dao.interfaces;

import java.util.List;
import mx.edu.uaz.ingsoft.poo2.persistencia.entidades.Usuario;

/**
 *
 * @author leo
 */
public interface IUsuario {

    Usuario add(Usuario unUsuario);
    void delete(Usuario unUsuario);
    Usuario update(Usuario unUsuario);
    Usuario findById(Integer id);
    List<Usuario> findAll();
}
