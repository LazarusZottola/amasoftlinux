package mx.edu.uaz.ingsoft.poo2.persistencia.dao.interfaces;

import java.util.List;
import mx.edu.uaz.ingsoft.poo2.persistencia.entidades.Compra;

public interface ICompra {

    Compra add(Compra unUsuario);
    void delete(Compra unUsuario);
    Compra update(Compra unTodo);
    Compra findById(Long id);
    List<Compra> findAll();
}
