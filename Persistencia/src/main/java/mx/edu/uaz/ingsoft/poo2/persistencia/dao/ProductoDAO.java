package mx.edu.uaz.ingsoft.poo2.persistencia.dao;

import javax.persistence.EntityManager;
import mx.edu.uaz.ingsoft.poo2.persistencia.dao.interfaces.EntidadBaseDAO;
import mx.edu.uaz.ingsoft.poo2.persistencia.entidades.Producto;

public class ProductoDAO extends EntidadBaseDAO<Producto>  {

    public ProductoDAO( EntityManager em) {
        super(Producto.class, em);
    }

}
