package mx.edu.uaz.ingsoft.poo2.persistencia.entidades;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table (name="Tabla_Compras")
public class Compra implements Serializable{

    private final static long serialVersionUID=1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(name = "precioFinal")
    private float precioFinal;
    @Column(name = "productos")
    private String productos;

    public Compra() {
    }

    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }

    public float getPrecioFinal() { 
        return precioFinal; 
    }
    public void setPrecioFinal(float precioFinal) {
        this.precioFinal = precioFinal;
    }

    public String getProductos() {
        return productos;
    }
    public void setProductos(String productos) { 
        this.productos = productos; 
    }

    @Override
    public String toString() {
                return "Compra{" + "id = " +  getId() + ", precioFinal = " + getPrecioFinal() + ", productos = " + getProductos() + "}";
    }
}
    