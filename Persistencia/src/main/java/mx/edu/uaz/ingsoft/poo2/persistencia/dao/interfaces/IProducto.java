package mx.edu.uaz.ingsoft.poo2.persistencia.dao.interfaces;

import java.util.List;
import mx.edu.uaz.ingsoft.poo2.persistencia.entidades.Producto;

public interface IProducto {

    Producto add(Producto unUsuario);
    void delete(Producto unUsuario);
    Producto update(Producto unTodo);
    Producto findById(Long id);
    List<Producto> findAll();

}
