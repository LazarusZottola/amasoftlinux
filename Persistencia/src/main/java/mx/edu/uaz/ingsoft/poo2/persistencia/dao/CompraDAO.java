package mx.edu.uaz.ingsoft.poo2.persistencia.dao;

import javax.persistence.EntityManager;
import mx.edu.uaz.ingsoft.poo2.persistencia.dao.interfaces.EntidadBaseDAO;
import mx.edu.uaz.ingsoft.poo2.persistencia.entidades.Compra;

public class CompraDAO extends EntidadBaseDAO<Compra> {

    public CompraDAO( EntityManager em) {
        super(Compra.class, em);
    }

}