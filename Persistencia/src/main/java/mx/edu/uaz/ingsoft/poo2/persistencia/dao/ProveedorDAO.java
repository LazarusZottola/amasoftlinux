package mx.edu.uaz.ingsoft.poo2.persistencia.dao;

import javax.persistence.EntityManager;
import mx.edu.uaz.ingsoft.poo2.persistencia.dao.interfaces.EntidadBaseDAO;
import mx.edu.uaz.ingsoft.poo2.persistencia.entidades.Proveedor;

/**
 * 
 * @author eibarra
 */
public class ProveedorDAO extends EntidadBaseDAO<Proveedor> {

    public ProveedorDAO( EntityManager em) {
        super(Proveedor.class, em);
    }
    
}