package mx.edu.uaz.ingsoft.poo2.persistencia.dao.interfaces;

import java.util.List;
import mx.edu.uaz.ingsoft.poo2.persistencia.entidades.Proveedor;

public interface IProveedor {

    Proveedor add(Proveedor unUsuario);
    void delete(Proveedor unUsuario);
    Proveedor update(Proveedor unTodo);
    Proveedor findById(Long id);
    List<Proveedor> findAll();
}
